'''
Created on Apr 15, 2018

@author: nirmalkumarraveendranath
'''
from pyspark.ml.classification import NaiveBayesModel
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from sklearn.decomposition import TruncatedSVD
from nltk import word_tokenize          
from nltk.stem.porter import PorterStemmer
import string
import HTMLParser
from pyspark.ml.feature import StringIndexer, StopWordsRemover, IndexToString
from pyspark.ml.feature import Tokenizer
from pyspark.ml.feature import HashingTF 
import numpy as np
import pandas as pd
from scipy.sparse.linalg import svds
   
   
stemmer = PorterStemmer()
html_parser = HTMLParser.HTMLParser()


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    text = html_parser.unescape(text)
    text = "".join([ch for ch in text if ch not in string.punctuation])
    tokens = word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def read_data(sparkConf):
    sc = SparkContext(conf=sparkConf)
    sc.setLogLevel("ERROR")
    sqlContext = SQLContext(sc)
    reviews_data = sqlContext.read.json('musicdata5/music_product_reviews1.json')
    sqlContext.registerDataFrameAsTable(reviews_data, 'review_table')
    df_result_reviews = reviews_data.toPandas()
    meta_data = sqlContext.read.json('musicdata5/music_product_metadata1.json')
    sqlContext.registerDataFrameAsTable(meta_data, 'meta_table')
    df_result_meta_data = meta_data.toPandas()
    return df_result_meta_data, df_result_reviews, meta_data, reviews_data, sqlContext

def compute_tf(data):
    categoryIndexer = StringIndexer(inputCol="overall", outputCol="label")
    categoryIndexerModel = categoryIndexer.fit(data)
    categoryIndexerModelpredictions = categoryIndexerModel.transform(data)
    tokenizer = Tokenizer(inputCol="reviewText", outputCol="words")
    tokens = tokenizer.transform(categoryIndexerModelpredictions)
    remover = StopWordsRemover(inputCol="words", outputCol="wordsfiltered")
    removal = remover.transform(tokens)
    hashingTF = HashingTF(inputCol="wordsfiltered", outputCol="features", numFeatures=10000)
    TF = hashingTF.transform(removal)
    return TF, categoryIndexerModel

def classify_all_reviews(product_indices, reviews_data):
    reviews_data_test = reviews_data[reviews_data['asin'].isin(product_indices)]
    reviews_data_test = reviews_data_test.filter(reviews_data_test["yes_helpful"] >= reviews_data_test["no_helpful"])
    naivebayes_model = NaiveBayesModel.load("/Users/nirmalkumarraveendranath/bda-workspace/Classification_Amazon_Reviews/musicmodule/models/naive_bayes_model")
    TF_test,categoryIndexerModel = compute_tf(reviews_data_test)
    predictions = naivebayes_model.transform(TF_test)
    categoryConverter = IndexToString(inputCol="prediction", outputCol="predCategory", labels=categoryIndexerModel.labels)
    categoryConverterModelpredictions = categoryConverter.transform(predictions)
    pd_df = categoryConverterModelpredictions.toPandas()
    spark_df = sqlContext.createDataFrame(pd_df)
    sqlContext.registerDataFrameAsTable(spark_df, 'review_table_classify')
    query = 'select asin,title, avg(predCategory) as overall from review_table_classify where overall>3.0 group by asin,title order by overall desc'
    classify = sqlContext.sql(query)
    df_classify = classify.toPandas()
    recommendations = []
    for product_indice in product_indices:
        classify_1 = df_classify.loc[df_classify['asin'] == product_indice]
        recommendations.append(classify_1.values.tolist())
    recommendations = sorted(recommendations,key=lambda x: x[0][2], reverse=True)
    for recommendation in recommendations:
        print(recommendation)
    return recommendations

def user_recommendations(predictions_df, reviewerID, df_reviews_data_test, r_r_df_reviews_data_test, df_result_meta_data, num_recommendations=5):
    reviwer = r_r_df_reviews_data_test[r_r_df_reviews_data_test.index == reviewerID]
    user_row_number = reviwer.reviewer_new_ID
    sorted_user_predictions = predictions_df.iloc[user_row_number]
    user_full = df_reviews_data_test[df_reviews_data_test['reviewerID'] == reviewerID].sort_values(['overall'], ascending=False)
    print 'User {0} has already rated {1} products.'.format(reviewerID, user_full.shape[0])
    print 'Recommending the highest {0} predicted ratings product not already rated.'.format(num_recommendations)
    
    sorted_user_predictions = pd.melt(sorted_user_predictions, var_name="asin", value_name="Predictions").sort_values(['Predictions'], ascending=False)
    recommendations = (df_result_meta_data[~df_result_meta_data['asin'].isin(user_full['asin'])].
         merge(pd.DataFrame(sorted_user_predictions).reset_index(), how = 'left',
               left_on = 'asin',
               right_on = 'asin').
         rename(columns = {str(user_row_number): 'Predictions'}).
         sort_values('Predictions', ascending = False).
                       iloc[:num_recommendations, :-1]
                      )
       
    return user_full[['asin','title']], recommendations[['asin','title']]
    

def get_collaborative_recommendations(product_indices,reviews_data,df_result_meta_data, title):
    
    reviews_data_test = reviews_data[reviews_data['asin'].isin(product_indices)]
    df_reviews_data_test = reviews_data_test.toPandas()
    df_reviews_data_test_selection = df_reviews_data_test[df_reviews_data_test['title'].str.contains(title)]
    print("Reviewer List who have reviewed the product "+title)
    print(df_reviews_data_test_selection[['reviewerID']])
    reviewerID = raw_input("To Which ReviewerID you want to get recommendations? ")
    r_df_reviews_data_test = df_reviews_data_test.pivot(index = 'reviewerID', columns ='asin', values = 'overall')
    r_df_reviews_data_test = r_df_reviews_data_test.fillna(r_df_reviews_data_test.mean())
    r_r_df_reviews_data_test = r_df_reviews_data_test.assign(reviewer_new_ID=[0 + i for i in xrange(len(r_df_reviews_data_test))])[['reviewer_new_ID'] + r_df_reviews_data_test.columns.tolist()]
    r = r_df_reviews_data_test.as_matrix()
    user_ratings_mean = np.mean(r, axis = 1)
    r_demeaned = r - user_ratings_mean.reshape(-1, 1)
    U, sigma, Vt = svds(r_demeaned, k = 48)
    sigma = np.diag(sigma)
    all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
    #A14OKOZW0PDTAD
    preds_df = pd.DataFrame(all_user_predicted_ratings, columns = r_df_reviews_data_test.columns)
    userfull, recomendations = user_recommendations(preds_df,reviewerID, df_reviews_data_test, r_r_df_reviews_data_test, df_result_meta_data)
    print("The products that are already rated by User")
    print(userfull)
    print("The products that are not rated by User")
    print(recomendations)


def calculate_tf_similarity(inputdata, column, n_components, feature_weight):
        tf = TfidfVectorizer(max_df=0.95, max_features=1000,
                                 min_df=0, stop_words='english',
                                 use_idf=True, tokenizer=tokenize, ngram_range=(1, 3))
        svdT = TruncatedSVD(n_components=n_components)
        tfidf_matrix = svdT.fit_transform(tf.fit_transform(inputdata[column]) * feature_weight)
        cosine_similarities_matrix = linear_kernel(tfidf_matrix, tfidf_matrix)
        return cosine_similarities_matrix

def calculat_cosine_similarities(cosine_similarities_title, cosine_similarities_description, cosine_similarities_brand, feature_weight, meta_data):
    cosine_similarities = ((cosine_similarities_title+cosine_similarities_description+cosine_similarities_brand)/sum(feature_weight))/3.0
    dict_row = {}
    for idx, row in meta_data.iterrows():
        similar_indices = cosine_similarities[idx].argsort()[:-50:-1]
        similar_items = [(cosine_similarities[idx][i],meta_data['title'][i], meta_data['asin'][i])
                         for i in similar_indices]
        flattened = []
        for similar_item in similar_items:
            flattened.append(similar_item)
        dict_row[row['asin']] = flattened
    return dict_row

def get_indices_product(recommendations, index):
    product_indices = [i[index] for i in recommendations]
    return product_indices

def get_indices_rating(recommendations, index):
    product_indices = [i[0][index] for i in recommendations]
    return product_indices

def get_recommendations(cosine_similarities, sqlContext, title):
    query = 'select * from meta_table where title like %s' % ('"%'+title+'%"')
    df_result_query = sqlContext.sql(query)
    asin = df_result_query.collect()[0]['asin']
    from operator import itemgetter
    recommendations = sorted(cosine_similarities[asin], key=itemgetter(0), reverse = True)
    print('First List of recommendations related to product')
    for recommendation in recommendations:
        print(recommendation)
    return recommendations, asin
    
if __name__ == '__main__':
    #input_product = input("Enter a product Title :")
    #input_reviewer = input("Enter a Reviewer ID :")
    #print('Title: '+input_product)
    #print('ReviewerID: '+input_reviewer)
    title = 'Muddy Water'
    sparkConf = SparkConf().setAppName('ReviewDataFrame')
    sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))
    df_result_meta_data, df_result_reviews, meta_data, reviews_data, sqlContext = read_data(sparkConf)
    feature_weight = [4, 2, 1]
    feature_title_similarity = calculate_tf_similarity(df_result_meta_data, 'title', 400, feature_weight[0])
    feature_descrption_similarity = calculate_tf_similarity(df_result_meta_data, 'description', 400, feature_weight[1])
    feature_brand_similarity = calculate_tf_similarity(df_result_meta_data, 'brand', 150, feature_weight[2])
    cosine_similarities = calculat_cosine_similarities(feature_title_similarity, feature_descrption_similarity, feature_brand_similarity, feature_weight, df_result_meta_data)
    recommendations, asin = get_recommendations(cosine_similarities, sqlContext, title)
    product_indices = get_indices_product(recommendations, 2)
    recommendations = classify_all_reviews(product_indices, reviews_data)
    product_indices = get_indices_rating(recommendations, 0)
    R_df_reviews_data_test = get_collaborative_recommendations(product_indices,reviews_data, df_result_meta_data, title)