'''
Created on Apr 4, 2018

@author: nirmalkumarraveendranath
'''
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.recommendation import ALS
from pyspark.ml.feature import StringIndexer
from pyspark.ml.evaluation import RegressionEvaluator
#books_review_json = 'Data/reviews_Books_5.json'
cds_vinyl_json = 'music/reviews_Digital_Music_5.json'
#electronics_json = 'Data/reviews_Electronics_5.json'
#movies_tv_json = 'Data/reviews_Movies_and_TV_5.json'




sparkConf = SparkConf().setAppName('ReviewDataFrame')

sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))


sc = SparkContext(conf = sparkConf)
sqlContext = SQLContext(sc)
reviews_data = sqlContext.read.json(cds_vinyl_json)
reviews_data.printSchema()
reviews_data.show(10)
stringIndexer_productID = StringIndexer(inputCol='asin',outputCol='int_asin')
model = stringIndexer_productID.fit(reviews_data)
reviews_ds = model.transform(reviews_data) 
stringIndexer_reviwerID = StringIndexer(inputCol='reviewerID',outputCol='int_reviewerID')
model = stringIndexer_reviwerID.fit(reviews_ds)
reviews_ds =  model.transform(reviews_ds) 
reviews_ds.show()
reviews_ds.describe().show()


print ("Number of partitions for the Product Reviews DataFrame: " + str(reviews_ds.rdd.getNumPartitions()))
print "Number of different ProductIds: " + str(reviews_ds.select('asin').distinct().count())
print "Number of different ReviewerIds: " + str(reviews_ds.select('reviewerID').distinct().count())
sqlContext.registerDataFrameAsTable(reviews_ds, 'review_table')
#df_result = sqlContext.sql("SELECT *, 100 * nb_ratings/matrix_size AS percentage FROM (SELECT nb_users, nb_movies, nb_ratings, nb_users * nb_movies AS matrix_size FROM ( SELECT COUNT(*) AS nb_ratings, COUNT(DISTINCT(asin) AS nb_movies, COUNT(DISTINCT(reviewerID)) AS nb_users FROM review_table ) )")
#df_result.show(10)

als = ALS(userCol="int_reviewerID", itemCol="int_asin", ratingCol="overall")


from pyspark.ml.tuning import CrossValidator, ParamGridBuilder

(trainingRatings, validationRatings) = reviews_ds.randomSplit([80.0, 20.0])
evaluator = RegressionEvaluator(metricName="rmse", labelCol="overall", predictionCol="prediction")

paramGrid = ParamGridBuilder().addGrid(als.rank, [1, 5, 10]).addGrid(als.maxIter, [7]).addGrid(als.regParam, [0.05, 0.1, 0.5]).build()

crossval = CrossValidator(estimator=als, estimatorParamMaps=paramGrid, evaluator=evaluator, numFolds=3)
#crossval.save('/Users/nirmalkumarraveendranath/bda-workspace/Classification_Amazon_Reviews/pipeline')
cvModel = crossval.fit(trainingRatings)
predictions = cvModel.transform(validationRatings)

print "The root mean squared error for our model is: " + str(evaluator.evaluate(predictions.na.drop()))
cvModel.save('models/alscvmodel')
