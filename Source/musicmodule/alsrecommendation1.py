'''
Created on Apr 8, 2018

@author: nirmalkumarraveendranath
'''

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.recommendation import ALS
from pyspark.ml.feature import StringIndexer
from pyspark.ml.evaluation import RegressionEvaluator

sparkConf = SparkConf().setAppName('ReviewDataFrame')
sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))

sc = SparkContext(conf = sparkConf)
sc.setLogLevel("ERROR")
sqlContext = SQLContext(sc)
reviews_data = sqlContext.read.json('musicdata5/music_product_reviews.json')
sqlContext.registerDataFrameAsTable(reviews_data, 'review_table')

query = 'select int_asin, int_reviewerID, avg(overall) as overall from review_table group by int_asin, int_reviewerID ' 
df_result = sqlContext.sql(query)
df_result.show(10)


(trainingRatings, testRatings) = reviews_data.randomSplit([80.0, 20.0])
als = ALS(userCol="int_reviewerID", itemCol="int_asin", ratingCol="overall")
model = als.fit(trainingRatings)
predictions = model.transform(testRatings)
avgRatings = reviews_data.select('overall').groupBy().avg().first()[0]
print "The average rating in the dataset is: " + str(avgRatings)

evaluator = RegressionEvaluator(metricName="rmse", labelCol="overall", predictionCol="prediction")
print "The root mean squared error for our model is: " + str(evaluator.evaluate(predictions.na.fill(avgRatings)))

model.save('models/alsmodel')