'''
Created on Apr 12, 2018

@author: nirmalkumarraveendranath
'''
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.recommendation import ALS
from pyspark.ml.feature import StringIndexer
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALSModel

sparkConf = SparkConf().setAppName('ReviewDataFrame')
sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))

sc = SparkContext(conf = sparkConf)
sc.setLogLevel("ERROR")
sqlContext = SQLContext(sc)
reviews_data = sqlContext.read.json('musicdata5/music_product_reviews.json')
reviews_data.printSchema()
reviews_data.show(10)

def recommendProducts(user, nbRecommendations,reviews_data):
    print("User"+user)
    alsmodel = ALSModel.load('models/alsmodel')
    dataSet = reviews_data.select("int_asin",'int_reviewerID','title').distinct()

    productsAlreadyRated = reviews_data.filter(reviews_data.reviewerName == user).select("int_asin", "int_reviewerID", "title")
    print(productsAlreadyRated)
    predictions = alsmodel.transform(dataSet.subtract(productsAlreadyRated)).dropna().orderBy("prediction", ascending=False).limit(nbRecommendations).select("int_asin","title", "prediction")
    predictions.show(truncate=False)
recommendProducts('Tim Janson', 10, reviews_data)