'''
Created on Apr 13, 2018

@author: nirmalkumarraveendranath
'''

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import StringIndexer
#books_review_json = 'Data/reviews_Books_5.json'
music_json = 'music/reviews_Digital_Music_5.json'
#electronics_json = 'Data/reviews_Electronics_5.json'
#movies_tv_json = 'Data/reviews_Movies_and_TV_5.json'
metadata_json = 'music/meta_Digital_Music.json'



sparkConf = SparkConf().setAppName('ReviewDataFrame')
sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))

sc = SparkContext(conf = sparkConf)
sc.setLogLevel("ERROR")
sqlContext = SQLContext(sc)
reviews_data = sqlContext.read.json(music_json)
reviews_data.printSchema()
reviews_data.show(10)
sqlContext.registerDataFrameAsTable(reviews_data, 'review_table')

meta_data = sqlContext.read.json(metadata_json)
meta_data.printSchema()
meta_data.show(10)
sqlContext.registerDataFrameAsTable(meta_data, 'meta_table')

query = 'select m.asin, r.reviewerID, r.reviewerName, r.helpful[0] as yes_helpful, r.helpful[1] as no_helpful , r.reviewText, r.summary, r.reviewTime, r.overall, m.title, m.price, m.description, m.brand from review_table r, meta_table m where m.asin=r.asin and m.title!="" and m.asin="B00005NHLY"'
reviews_data = sqlContext.sql(query)



stringIndexer_productID = StringIndexer(inputCol='asin',outputCol='int_asin')
model = stringIndexer_productID.fit(reviews_data)
reviews_ds = model.transform(reviews_data) 
stringIndexer_reviwerID = StringIndexer(inputCol='reviewerID',outputCol='int_reviewerID')
model = stringIndexer_reviwerID.fit(reviews_ds)
reviews_ds =  model.transform(reviews_ds) 
print('review_table, metatable, with int')
reviews_ds.printSchema()
reviews_ds.show(20)
#reviews_ds.write.format('json').save('musicdata5/music_product_reviews1.json')


query = 'select distinct m.asin, m.title, m.price, m.description, m.brand, m.related.also_bought as also_bought, m.related.also_viewed as also_viewed, m.related.bought_together as bought_together, m.related.buy_after_viewing as buy_af_viewing,m.categories from review_table r, meta_table m where m.asin=r.asin and m.title!="" and m.description!="" and m.brand!="" and m.asin="B00005NHLY"'
reviews_data = sqlContext.sql(query)
stringIndexer_productID = StringIndexer(inputCol='asin',outputCol='int_asin')
model = stringIndexer_productID.fit(reviews_data)
reviews_ds = model.transform(reviews_data) 
reviews_ds = reviews_ds.na.fill({'description': 'NA', 'title': '', 'brand': '', 'price': 0.0})
reviews_ds.printSchema()
reviews_ds.show(10)
reviews_ds.write.format('json').save('musicdata5/music_product_metadata1.json')


