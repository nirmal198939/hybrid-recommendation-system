'''
Created on Apr 16, 2018

@author: nirmalkumarraveendranath
'''

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, SQLContext
import scikitplot as skplt
import matplotlib.pyplot as plt
from musicmodule.reviews import logisticregression 
import decisiontreeclassifier
import linearsvmclassifier
import randomforestclassifier
import naviebayesclassifier

sparkConf = SparkConf().setAppName('ReviewDataFrame')
sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))


def read_data():
    sc = SparkContext(conf=sparkConf)

    sc.setLogLevel("ERROR")
    sqlContext = SQLContext(sc)
    reviews_data = sqlContext.read.json('../musicdata5/music_product_reviews.json')
    
    reviews_dataDF = reviews_data.toPandas()
    reviews_dataDF['textlength'] = reviews_dataDF['reviewText'].apply(len)
    reviews_dataDF = reviews_dataDF[(reviews_dataDF['overall'] == 1.0) | (reviews_dataDF['overall'] == 5.0)]
    reviews_data = sqlContext.createDataFrame(reviews_dataDF)
    return reviews_data


def _get_labels(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    return probability_array


def _compute_all_vectors(data):
    train_data, test_data = data.randomSplit([0.8, 0.2])
    logistic_prob = logisticregression._compute_vectors(train_data, test_data)
    decision_prob = decisiontreeclassifier._compute_vectors(train_data, test_data)
    randomforest_prob = randomforestclassifier._compute_vectors(train_data, test_data)
    naivebayes_prob = naviebayesclassifier._compute_vectors(train_data, test_data)
    clf_names = ['Logistic Regression', 'Decision Tree', 'Random Forest', 'Naive Bayes' ]
    
    probas_list = [ _get_labels(logistic_prob), _get_labels(decision_prob), _get_labels(randomforest_prob), _get_labels(naivebayes_prob)]
    
    skplt.metrics.plot_calibration_curve(naivebayes_prob['label'], probas_list, clf_names)
    plt.show()


data = read_data()
_compute_all_vectors(data)
