'''
Created on Apr 16, 2018

@author: nirmalkumarraveendranath
'''

from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml import Pipeline
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import StringIndexer, StopWordsRemover, IndexToString
from pyspark.ml.feature import Tokenizer
from pyspark.ml.feature import HashingTF 
import scikitplot as skplt
import matplotlib.pyplot as plt
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

def read_data(sparkConf):
    sc = SparkContext(conf = sparkConf)   
    sc.setLogLevel("ERROR")
    sqlContext = SQLContext(sc)
    reviews_data = sqlContext.read.json('../musicdata5/music_product_reviews.json')
    
    reviews_dataDF = reviews_data.toPandas()
    reviews_dataDF['textlength'] = reviews_dataDF['reviewText'].apply(len)
    
    
    reviews_dataDF = reviews_dataDF[(reviews_dataDF['overall'] == 1.0) | (reviews_dataDF['overall'] == 5.0) | (reviews_dataDF['overall'] == 3.0)]
    reviews_data = sqlContext.createDataFrame(reviews_dataDF)
    return reviews_data
def confusion_matrix(pd_df):
    pred_category = pd_df['predCategory']
    overall =  pd_df['overall'].apply(str)
    skplt.metrics.plot_confusion_matrix(overall, pred_category, normalize=True)
    plt.show() 

def roc_curve(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    original_label = pd_df['label']
    skplt.metrics.plot_roc_curve(original_label, probability_array)
    plt.show()

def recall_prep(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    original_label = pd_df['label']
    skplt.metrics.plot_precision_recall_curve(original_label, probability_array)
    plt.show()

def ks_static_plot(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    original_label = pd_df['label']
    skplt.metrics.plot_ks_statistic(original_label, probability_array)
    plt.show()

def cumulative_gain(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    original_label = pd_df['label']
    skplt.metrics.plot_cumulative_gain(original_label, probability_array)
    plt.show()

def lift_curve(pd_df):
    probability_array = []
    for dense_vector in pd_df['probability']:
        probability_array.append(dense_vector.toArray().tolist())
    original_label = pd_df['label']
    skplt.metrics.plot_lift_curve(original_label, probability_array)
    plt.show()
    
def _compute_vectors(train_data,test_data):
    categoryIndexer = StringIndexer(inputCol="overall", outputCol="label")
    categoryIndexerModel = categoryIndexer.fit(train_data)
    tokenizer = Tokenizer(inputCol="reviewText", outputCol="words")
    remover = StopWordsRemover(inputCol="words", outputCol="wordsfiltered")
    hashingTF = HashingTF(inputCol="wordsfiltered", outputCol="features", numFeatures=10000)
    dc = DecisionTreeClassifier()
    categoryConverter = IndexToString(inputCol="prediction", outputCol="predCategory", labels=categoryIndexerModel.labels)
    pipeline = Pipeline(stages=[categoryIndexer, tokenizer, remover, hashingTF, dc, categoryConverter])
    model = pipeline.fit(train_data)
    pr = model.transform(test_data)
    pd_df = pr.toPandas()
    evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction",
                                              metricName="accuracy")
    accuracy = evaluator.evaluate(pr)
    print "Model Accuracy: ", accuracy
    evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction",
                                              metricName="f1")
    accuracy = evaluator.evaluate(pr)
    print "Model F1: ", accuracy
    evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction",
                                              metricName="weightedPrecision")
    accuracy = evaluator.evaluate(pr)
    print "Model precision: ", accuracy
    evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction",
                                              metricName="weightedRecall")
    accuracy = evaluator.evaluate(pr)
    print "Model recall: ", accuracy
    pr.select("label","reviewText", "prediction","overall", "predCategory").show(100)
      
    return pd_df
    #model.save('/Users/nirmalkumarraveendranath/bda-workspace/Classification_Amazon_Reviews/musicmodule/models/musicclassifynaivebayes')

if __name__ == '__main__':
    sparkConf = SparkConf().setAppName('ReviewDataFrame')
    sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))
    data = read_data(sparkConf)
    train_data, test_data = data.randomSplit([0.8, 0.2])
    pd_df = _compute_vectors(train_data,test_data)
    confusion_matrix(pd_df)
    roc_curve(pd_df)
    recall_prep(pd_df)