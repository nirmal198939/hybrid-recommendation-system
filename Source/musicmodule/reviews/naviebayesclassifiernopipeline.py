'''
Created on Apr 16, 2018

@author: nirmalkumarraveendranath
'''

from pyspark.ml.classification import NaiveBayes as mlNaiveBayes
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import StringIndexer, StopWordsRemover, IndexToString
from pyspark.ml.feature import Tokenizer
from pyspark.ml.feature import HashingTF 


def read_data(sparkConf):
    sc = SparkContext(conf = sparkConf)
    sc.setLogLevel("ERROR")
    sqlContext = SQLContext(sc)
    reviews_data = sqlContext.read.json('../musicdata5/music_product_reviews.json')
    reviews_dataDF = reviews_data.toPandas()
    reviews_dataDF['textlength'] = reviews_dataDF['reviewText'].apply(len)
    reviews_dataDF = reviews_dataDF[(reviews_dataDF['overall'] == 1.0) | (reviews_dataDF['overall'] == 3.0) | (reviews_dataDF['overall'] == 5.0)]
    reviews_data = sqlContext.createDataFrame(reviews_dataDF)
    return reviews_data

def compute_tf(data):
    categoryIndexer = StringIndexer(inputCol="overall", outputCol="label")
    categoryIndexerModel = categoryIndexer.fit(data)
    categoryIndexerModelpredictions = categoryIndexerModel.transform(data)
    tokenizer = Tokenizer(inputCol="reviewText", outputCol="words")
    tokens = tokenizer.transform(categoryIndexerModelpredictions)
    remover = StopWordsRemover(inputCol="words", outputCol="wordsfiltered")
    removal = remover.transform(tokens)
    hashingTF = HashingTF(inputCol="wordsfiltered", outputCol="features", numFeatures=10000)
    TF = hashingTF.transform(removal)
    return TF, categoryIndexerModel

def compute_text(data, test_data):
    TF_train,categoryIndexerModel = compute_tf(data)
    TF_test,categoryIndexerModel = compute_tf(test_data)
    nb = mlNaiveBayes(smoothing=1.0, modelType="multinomial")
    model = nb.fit(TF_train)
    predictions = model.transform(TF_test)
    categoryConverter = IndexToString(inputCol="prediction", outputCol="predCategory", labels=categoryIndexerModel.labels)
    categoryConverterModelpredictions = categoryConverter.transform(predictions)
    pd_df = categoryConverterModelpredictions.toPandas()
    model.write().overwrite().save("../models/naive_bayes_model")
    return pd_df

if __name__ == '__main__':
    sparkConf = SparkConf().setAppName('ReviewDataFrame')
    sparkConf = (sparkConf.setMaster('local[*]')
        .set('spark.executor.memory', '7G')
        .set('spark.driver.memory', '45G')
        .set('spark.driver.maxResultSize', '10G'))
    data = read_data(sparkConf)
    train_data, test_data = data.randomSplit([0.8, 0.2])
    pd_df = compute_text(train_data,test_data)
  
